$(document).ready(function(){
      $("#submitBtn").click(function(){
        var company_or_title_val = $("#title").val()
        var city_name_val = $("#city").val()
        var office_val =  $("#office_checkbox").prop("checked")
        var remote_val = $("#remote_checkbox").prop("checked")
        var min_pay_val = $("#min_pay").val()
        var max_pay_val = $("#max_pay").val()
        console.log("sending: " +company_or_title_val + " with city " + city_name_val + " office" + office_val + " remote" + remote_val + " min" + min_pay_val + " max" + max_pay_val  )
        $("#jobs-container").empty()
        $.ajax({
            url: 'job_updater.php',
            type: 'POST',
            dataType: 'json',
            data: {  company_or_title: company_or_title_val, city_name: city_name_val, office: office_val, remote: remote_val, min_pay:min_pay_val, max_pay: max_pay_val},
            success: function(output){
                console.log(output)
                $("#jobs-container").prepend("<div id='target' class='m-2 p-2 rounded bg-light border border-dark'></div>")
                $("#target").append("<h4>Вакансия: "+output.res_name+"</h4>")
                $("#target").append("<p class='text-muted'>"+output.res_company+"</p>")
                $("#target").append("<p class='text'>От: "+output.res_min+"</p>")
                $("#target").append("<p class='text'>До:"+output.res_max+"</p>")
                $("#target").append("<p class='text'>Удаленка: "+output.res_remote+"</p>")
                $("#target").append("<p class='text'>Офис:"+output.res_office+"</p>")
                $("#target").append("<p class='text'>Город: "+output.res_city+"</p>")
                $("#target").removeAttr( "id" )
              }
        });
      });
});