<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Вакансии</title>
    <link href="bootstrap.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
</head>
<header>
    <div class="mb-2 bg-light border border-dark">
        <h3 class="text-center text-primary ">Вакансии</h3>
    </div>
</header>
<body class=" container">
    <div id="note-add" class=" m-3 p-3 pb-5 rounded bg-light border border-dark">
        <h2 class="text-center bg-light">Поиск вакансий</h2>
        <form>
            <div class="form-group">
                <input type="text" class="form-control" id="title" placeholder="Вакансия или компания">
            </div>
            <div class="form-group">
            <div class="form-row mt-3">
                    <div class="col">
                        <select class="custom-select" id="city">
                            <option value="" disabled selected>Город</option>
                            <option>Пермь</option>
                            <option>Москва</option>
                            <option>Санкт-Петербург</option>
                        </select>
                    </div>
                    <div class="col input-group-text">
                            <label for="office_checkbox"> Офис</label>
                            <input type="checkbox" id="office_checkbox">
                    </div>
                    <div class="col input-group-text">
                            <label for="remote_checkbox"> Удаленка</label>
                            <input type="checkbox" id="remote_checkbox">
                    </div>
                    <div class="col input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">От</span>
                        </div>
                        <input type="text" class="form-control" id="min_pay" aria-describedby="basic-addon3">
                    </div>
                    <div class="col input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">до</span>
                        </div>
                        <input type="text" class="form-control" id="max_pay" aria-describedby="basic-addon3">
                    </div>
                    <div class="col ">
                            <p>руб/мес</p>
                    </div>
                        
                    
                </div>
            </div>
            <button id="submitBtn" type="button" class="btn btn-primary float-right">Поиск</button>
        </form>
    </div>
    <div id="jobs-container">
        <?php
            require_once 'config.php';
            $query = mysqli_query($link,"SELECT * FROM openings INNER JOIN citys on openings.city_id=citys.city_id");
            while ($oneJob =  mysqli_fetch_array($query)){             
                    echo 
                    '<div class="m-2 p-2 rounded bg-light border border-dark">
                        <h4>'.$oneJob['name'].'</h4>
                        <p class="text-muted">'.$oneJob['company'].'</p>
                        <p class="text"> Удаленка: '.$oneJob['remote'].'</p>
                        <p class="text"> Офис: '.$oneJob['office'].'</p>
                        <p class="text"> Город:'.$oneJob['city_name'].'</p>
                        <p class="text"><b>Зарплата</b>: от: '.$oneJob['pay_min'].' до: '.$oneJob['pay_max'].'</p>
                    </div>'; 
                }         
        ?>
    </div>
</body>
<footer>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="job_script.js"></script>
</footer>
</html>
    
