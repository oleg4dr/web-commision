<?php
    require_once 'config.php';

    $p1 = $_REQUEST['company_or_title'];
    $p2 = $_REQUEST['city_name'];
    $p3 = $_REQUEST['office'];
    $p4 = $_REQUEST['remote'];
    $p5 = $_REQUEST['min_pay'];
    $p6 = $_REQUEST['max_pay'];
    $query = mysqli_query($link, "SELECT * FROM openings INNER JOIN citys ON openings.city_id=citys.city_id  
    WHERE city_name=\"$p2\" AND 
    pay_min>=$p5 AND
    pay_max>=$p6  AND
    remote=$p4 AND
    office=$p3 AND 
    (name=\"$p1\" OR company=\"$p1\")
    ORDER BY id DESC LIMIT 1");
    $res =  mysqli_fetch_array($query);

    $p4 = ($res['remote'] == 1) ? "да" : "нет";
    $p3 = ($res['office'] == 1) ? "да" : "нет";

    die(json_encode(array(
        'message'     => 'найденные работы',
        'res_name'     => $res['name'],
        'res_company'     => $res['company'],
        'res_min'     => $res['pay_min'],
        'res_max'     => $res['pay_max'],
        'res_remote'     => $p4,
        'res_office'     => $p3,
        'res_city'     => $res['city_name'],
    )));
?>